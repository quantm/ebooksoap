/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module:
 * @version:
 * @date: 12/1/13 9:36 PM
 */
$(function(){
    (new Chapter()).execute();
     function Chapter(){
         var $this = this;

         $this.execute  = function(){
             add_route('/chapter/add', $this.update)
             add_route('/chapter/edit', $this.update)
         }

         $this.update   = function(){
               $('#chapter-form button').click(function(){
                    var data = get_form_data('#chapter-form');
                  //data.content = CKEDITOR.instances.editor1.getData();
                   $.post(location.href, data)
                       .done(function(d){
                            if(d == 'SUCCESS'){
                                var href = location.href;
                                if(href.indexOf('/add') != -1)
                                    location.href = href.replace(/\/add/,'/all');
                                else
                                    $('#chapter-form').message_box('success', 'Lưu lại thành công');
                            }else{
                                $('#chapter-form').message_box('danger', d);
                            }
                       });
               });
         }
     }
})
