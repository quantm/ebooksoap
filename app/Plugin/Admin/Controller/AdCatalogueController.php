<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/27/13 7:22 PM
*/
class AdCatalogueController extends AdminAppController{
    public $layout = "admin";
    public $uses = array('Catalogue');
    public function all(){
        $catalogues = $this->Catalogue->generateTreeList(null, null, null, '---');
        if($this->request->isPost()){
            $data = $this->data['Catalogue'];
            if(!empty($data['parent_id'])){
                $data['name'] = $catalogues[$data['parent_id']] . ' - ' . $data['name'];
            }
            $this->Catalogue->set($data);
            if($this->Catalogue->validates()){
                if($this->Catalogue->save()){
                    $this->Session->setFlash('Thêm mục mới thành công', 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('controller' => 'AdCatalogue', 'action' => 'all'));
                };

            }
        }
        $this->set('catalogues', $catalogues);
        $this->set('title', 'Tất cả các mục');

    }

    public function delete($id = null){
        $this->Catalogue->delete($id);
        $this->redirect(array('controller' => 'AdCatalogue', 'action' => 'all'));
    }
}