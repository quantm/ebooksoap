<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/27/13 6:37 PM
*/

 class AdEbookController extends AdminAppController{

    public $layout = "admin";
    public $uses = array('Author', 'Catalogue', 'Ebook');
    public function add(){
        if($this->request->isPost()){
            $data = $this->data['Ebook'];
            $this->Ebook->set($data);
            if($this->Ebook->validates()){
                if($this->Ebook->save()){
                    $this->redirect(array('controller' => 'AdEbook', 'action' => 'all'));
                };
            }
        }
        $authors = $this->Author->GetAuthorList();
        $catalogues = $this->Catalogue->generateTreeList(null, null, null, '---');
        $title = 'Thêm sách mới';
        $this->set(compact('authors', 'catalogues', 'title'));
    }

     public function edit($id = null){
         $ebook = $this->Ebook->read(null, $id);
         if(empty($ebook))
             throw new NotFoundException();
         $this->set('ebook', $ebook);
         if($this->request->isPost()){
             $this->Ebook->set($this->data);
             if($this->Ebook->validates()){
                 if($this->Ebook->save()){
                     $this->redirect(array('controller' => 'AdEbook', 'action' => 'all'));
                 };
             }
             //$ebook = $this->data;
             $this->set('ebook', $this->data);
         }
         $authors = $this->Author->GetAuthorList();
         $catalogues = $this->Catalogue->generateTreeList(null, null, null, '---');
         $title = 'Thêm sách mới';


         $this->set(compact('authors', 'catalogues', 'title'));
     }

     public function all(){
         $this->Paginator->settings = array(
             'limit' => 10,
             'order' => 'Ebook.modified desc'
         );
         $ebooks = $this->Paginator->paginate('Ebook');
         //pr($ebooks);
         //die;
         $this->set('ebooks', $ebooks);
         $this->set('title', 'Tất cả sách');
     }

     public function delete($id = null){
         $this->Ebook->delete($id);
         $this->redirect(array('controller' => 'AdEbook', 'action' => 'all'));
     }
}
