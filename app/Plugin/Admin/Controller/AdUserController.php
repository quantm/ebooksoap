<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 12/2/13 8:05 PM
*/
class AdUserController extends AdminAppController{
    public $layout = 'admin';
    public $uses = array('User');
    public function all(){
        $this->Paginator->settings = array(
            'limit' => 10,
            'order' => 'User.modified desc'
        );
        $users = $this->Paginator->paginate('User');

        $this->set('groups', $this->User->groups);
        $this->set('users', $users);
        $this->set('title', 'Tất cả người dùng');
    }

    public function add(){
        if($this->request->isPost()){
            $this->User->set($this->data);
            if($this->User->validates()){
                if($this->User->save()){
                    $this->redirect(array('controller' => 'AdUser', 'action' => 'all'));
                };
            }
        }

        $this->set('groups', $this->User->groups);
        $this->set('title', 'Thêm người dùng mới');
    }

    public function edit($id){
        $user = $this->User->read(null, $id);
        $this->set('user', $user);
        if($this->request->isPost()){
            $this->set('user', $this->data);
            $this->User->set($this->data);
            if($this->User->validates()){
                //$this->User->id = $id;
                if($this->User->save()){
                    $this->Session->setFlash('Cập nhật thành công', 'default', array('class' => 'alert alert-success'));
                    //$this->redirect(array('controller' => 'AdUser', 'action' => 'edit', $id));
                };
            }

        }
        $this->User->validator()->remove('password', 'required');

        $this->set('groups', $this->User->groups);

        $this->set('title', 'Chỉnh sửa: ' . $user['User']['username']);
    }
}