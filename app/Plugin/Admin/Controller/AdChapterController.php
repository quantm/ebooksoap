<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 12/1/13 8:42 PM
*/
class AdChapterController extends AdminAppController{
    public $layout = "admin";
    public $uses = array('Ebook', 'Chapter');

    public function all($ebook_id){
        $chapters = $this->Chapter->GetChapterByEbookID($ebook_id, array('chapter_id', 'name', 'description', 'modified', 'created'));
        $ebook   = $this->Ebook->read(array('title', 'Author.name', 'Catalogue.name'), $ebook_id);
        $title = 'Quản lí: ' . $ebook['Ebook']['title'];
        $this->set(compact('chapters', 'ebook', 'title'));

    }

    public function add($ebook_id){
        if($this->request->isPost()){
            $data = array(
                'ebook_id'      => $ebook_id,
                'name'          => $this->data['name'],
                'description'   => $this->data['description'],
                'content'       => $this->data['content']
            );
            $this->Chapter->set($data);
            if($this->Chapter->validates()){
                if($this->Chapter->save()){
                    $this->Ebook->UpdateChapterNum($ebook_id, $this->Chapter);
                    echo 'SUCCESS';
                }
            }else{
                foreach($this->Chapter->validationErrors as $e){
                    echo $e[0] . '<br>';
                }
            }
            exit;
        }

        $ebook   = $this->Ebook->read(array('title', 'Author.name', 'Catalogue.name'), $ebook_id);

        $title = 'Thêm chương: ' . $ebook['Ebook']['title'];
        $this->set(compact( 'ebook', 'title'));
    }

    public function edit($ebook_id, $id){
        if($this->request->isPost()){
            $data = array(
                'ebook_id'      => $ebook_id,
                'name'          => $this->data['name'],
                'description'   => $this->data['description'],
                'content'       => $this->data['content']
            );
            $this->Chapter->id = $id;
            $this->Chapter->set($data);
            if($this->Chapter->validates()){
                if($this->Chapter->save())
                    echo 'SUCCESS';
            }else{
                foreach($this->Chapter->validationErrors as $e){
                    echo $e[0] . '<br>';
                }
            }
            exit;
        }

        $chapter = $this->Chapter->read(null, $id);
        $ebook   = $this->Ebook->read(array('title', 'Author.name', 'Catalogue.name'), $ebook_id);
        if($chapter['Chapter']['ebook_id'] != $ebook_id)
            throw new BadRequestException();


        $title = 'Chỉnh sửa: ' . $ebook['Ebook']['title'];
        $this->set(compact('chapter', 'ebook', 'title'));
    }

    public function delete($ebook_id, $id = null){
        $chapter = $this->Chapter->read(array('ebook_id'), $id);
        if($chapter['Chapter']['ebook_id'] != $ebook_id)
            throw new BadRequestException();

        $this->Chapter->delete($id);
        $this->Ebook->UpdateChapterNum($ebook_id, $this->Chapter);
        $this->redirect(array('controller' => 'AdChapter', 'action' => 'all', $ebook_id));
    }

    public function beforeFilter(){
        parent::beforeFilter();
		//'/lib/ckeditor/full/ckeditor.js',
        $this->set('scripts', array('Admin.chapter.js'));
    }
}