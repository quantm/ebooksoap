<?php 
	class AdminAppController extends AppController {
        public $components = array(
            'Paginator',
            'Session',
            'Cookie',
            'Auth' => array(
                'loginAction' => array(
                    'plugin' => 'Home',
                    'controller' => 'User',
                    'action' => 'login'
                ),
                'authError' => 'Did you really think you are allowed to see that?'
            )
        );
        function beforeFilter(){
            parent::beforeFilter();
            $this->Auth->allow();
        }
	}
?>