<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module:
 * @version:
 * @date: 11/27/13 7:22 PM
 */
class AdAuthorController extends AdminAppController{
    public $layout = "admin";
    public $uses = array('Author');
    public function all(){
        if($this->request->isPost()){
            $data = $this->data['Author'];
            $this->Author->set($data);
            if($this->Author->validates()){
                if($this->Author->save()){
                    $this->Session->setFlash('Thêm tác giả mới thành công', 'default', array('class' => 'alert alert-success'));
                    $this->redirect(array('controller' => 'AdAuthor', 'action' => 'all'));
                };

            }
        }
        $authors = $this->Author->find('all');
        $this->set('authors', $authors);
        $this->set('title', 'Tác giả');
    }

    public function delete($id = null){
        $this->Author->delete($id);
        $this->redirect(array('controller' => 'AdAuthor', 'action' => 'all'));
    }
}