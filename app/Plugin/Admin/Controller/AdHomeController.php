<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 12/6/13 1:10 PM
*/
class AdHomeController extends AdminAppController{
    public $autoLayout = false;
    public $autoRender = false;
    public $uses  = array('Ebook', 'User', 'UserEbook');

    public function index(){
        $this->redirect(array('controller' => 'AdEbook', 'action' => 'all'));
    }


}