<div class="c-container">
    <?php echo $this->Session->flash();?>
    <?php echo $this->Form->create('Author', array('class' => 'small-form'));?>
    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nhập tên tác mới', 'placeholder' => 'Nhập tên mục mới'))?>
    <?php echo $this->Form->input('description', array('type' => 'textarea','class' => 'form-control', 'label' => 'Mô tả'))?>
    <?php echo $this->Form->submit('Thêm tác giả', array('class' => 'btn btn-primary'));?>
    <?php echo $this->Form->end();?>

    <ul class="tags">
        <?php foreach($authors as $author) : $a = $author['Author'];?>
            <li><a data-action="delete" data-text= "<?php _h($a['name'])?>" href="<?php echo Router::url(array('controller' => 'AdAuthor', 'action' => 'delete', $a['author_id']))?>" title="<?php _h($a['description'])?>"><?php _h($a['name']);?></a></li>
        <?php endforeach;?>
    </ul>
</div>