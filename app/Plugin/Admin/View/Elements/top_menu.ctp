<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Fashion Shop</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="tm-home"><a href="#">Home</a></li>
                <li <?php is_active( Router::url(null, true), '/ebooks');?>><?php echo $this->Html->link('Ebook', array('controller' => 'AdEbook', 'action' => 'all'));?></li>
                <li <?php is_active( Router::url(null, true), '/catalogue');?>><?php echo $this->Html->link('Phân loại', array('controller' => 'AdCatalogue', 'action' => 'all'));?></li>
                <li <?php is_active( Router::url(null, true), '/author');?>><?php echo $this->Html->link('Tác giả', array('controller' => 'AdAuthor', 'action' => 'all'));?></li>
                <li <?php is_active( Router::url(null, true), '/user');?>><?php echo $this->Html->link('User', array('controller' => 'AdUser', 'action' => 'all'));?></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
        </div><!--/.nav-collapse -->
    </div> <!--.container-->
</div>