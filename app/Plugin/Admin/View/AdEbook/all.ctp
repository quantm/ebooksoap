<div class="c-container">
    <a class="btn btn-info" href="<?php echo Router::url(array('controller' => 'AdEbook', 'action' => 'add'))?>">Thêm sách</a>
    <br><br>
    <table class="table table-bordered table-hover">
        <tr>
            <th>#</th>
            <th>Tiêu đề</th>
            <th>Tác giả</th>
            <th>Phân loại</th>
            <th>Nhà xuất bản</th>
            <th>Trạng thái</th>
            <th>Chapters</th>
            <th>Pages</th>
            <th>Lượt xem</th>
            <th>!</th>
        </tr>

        <?php foreach($ebooks as $ebook) : $e = $ebook['Ebook']?>
            <tr>
                <td><?php _h($e['ebook_id'])?></td>
                <td><a href="<?php echo Router::url(array('controller' => 'AdChapter', 'action' => 'all', $e['ebook_id']))?>"><?php _h($e['title'])?></a></td>
                <td><?php _h($ebook['Author']['name'])?></td>
                <td><?php _h($ebook['Catalogue']['name'])?></td>
                <td><?php _h($e['publisher'])?></td>
                <td><?php _h($e['status'] ? 'Đã xuất bản' : 'Nháp')?></td>
                <td><?php _n($e['chapters_number'])?></td>
                <td><?php _n($e['pages_number'])?></td>
                <td><?php _n($e['views_number'])?></td>
                <td>
                    <?php echo $this->Html->link('Sửa', array('controller' => 'AdEbook', 'action' => 'edit', $e['ebook_id']))?>
                    |
                   <a href="<?php echo Router::url(array('controller' => 'AdEbook', 'action' => 'delete', $e['ebook_id']))?>" data-text="<?php _h($e['title'])?>" data-action="delete">Xóa</a>
                </td>
            </tr>
        <?php endforeach?>

    </table>
    <?php echo $this->element('_paging')?>
</div>