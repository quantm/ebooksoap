<div class="c-container ebook">
   <?php echo $this->Form->create('Ebook', array('class' => 'small-form', 'id' => 'ebook-form'));?>

        <button class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span>Lưu lại</button>
    <div style="margin-top: 20px;">
    <div class="form-panel">
    <?php echo $this->Form->input('title', array(
            'class' => 'form-control',
            'label' => 'Nhập tiêu đề sách',
            'placeholder' => 'Nhập tiêu đề'))?>
        <?php echo $this->Form->input('description', array(
            'class' => 'form-control',
            'label' => 'Nhập mô tả',
            'type' => 'textarea',
            'style' => 'height: 103px;',
            'placeholder' => 'Nhập tiêu đề'))?>

        <?php echo $this->Form->input('translator', array(
            'class' => 'form-control',
            'label' => 'Nhập tên dịch giả(nếu có)',
            'placeholder' => 'Nhập tên dịch giả'))?>

        <?php echo $this->Form->input('publisher', array(
            'class' => 'form-control',
            'label' => 'Nhập tên nhà xuất bản',
            'placeholder' => 'Nhà xuất bản'))?>

    </div>
    <div class="form-panel">
        <?php echo $this->Form->input('status', array(
            'class' => 'form-control',
            'label' => 'Trạng thái',
            'options' => array('Nháp', 'Xuất bản'),
            'placeholder' => 'Trạng thái'))?>
        <?php echo $this->Form->input('pages_number', array(
            'class' => 'form-control',
            'label' => 'Số trang',
            'type' => 'number',
            'value' => 1,
            'data-min-value' => 1,
            'data-max-value' => 50000,
            'placeholder' => 'Trạng trang'))?>
        <?php /*echo $this->Form->input('chapters_number', array(
            'class' => 'form-control',
            'label' => 'Số chương',
            'type' => 'number',
            'data-min-value' => 1,
            'value' => 1,
            'data-max-value' => 50000,
            'placeholder' => 'Số chương'))*/?>

        <?php echo $this->Form->input('author_id', array(
            'class' => 'form-control',
            'label' => 'Tác giả',
            'options' => $authors,
            'empty' => 'Không có',
            'placeholder' => 'Nhà xuất bản'))?>

        <?php echo $this->Form->input('catalogue_id', array(
            'class' => 'form-control',
            'label' => 'Chọn mục',
            'options' => $catalogues,
            'empty' => 'Không có',
            'placeholder' => 'Nhà xuất bản'))?>
    </div>
    </div>
    <?php echo $this->Form->end();?>
</div>