<div class="c-container">
    <h1><a href="<?php echo Router::url(array('controller' => 'AdChapter', 'action' => 'all', $ebook['Ebook']['ebook_id']))?>"><?php _h($ebook['Ebook']['title'])?></a><small> - <?php _h($ebook['Author']['name'])?></small></h1>
    <p>Thể loại: <?php _h($ebook['Catalogue']['name'])?></p>
    <form id="chapter-form">
        <button  class="btn btn-info" type="button"><span class="glyphicon glyphicon-ok"></span>Lưu lại</button><br><br>
        <label for="name">Tên chương</label>
        <input type="text" required style="width: 500px;" name="name" id="name" class="form-control" placeholder="Tên chương"><br>
        <label for="description">Mô tả</label>
        <textarea style="width: 400px;" name="description" class="form-control" placeholder="Nhập mô tả"></textarea><br>
        <textarea style="" class="ckeditor" cols="80" id="editor1" name="content" rows="10"></textarea>
    </form>
</div>