<div class="c-container">
    <h1><?php _h($ebook['Ebook']['title'])?><small> - <?php _h($ebook['Author']['name'])?></small></h1>
    <p>Thể loại: <?php _h($ebook['Catalogue']['name'])?></p>
    <a href="<?php echo Router::url(array('controller' => 'AdChapter', 'action' => 'add', $ebook['Ebook']['ebook_id']))?>" class="btn btn-info">Thêm chương mới</a><br><br>
    <table class="table table-bordered table-hover">
        <tr>
            <th>STT</th>
            <th>Tên chương</th>
            <th>Mô tả</th>
            <th>Ngày sửa</th>
            <th>Ngày thêm</th>
            <th>!</th>
        </tr>
        <?php foreach($chapters as $i => $chapter) : $c = $chapter['Chapter']?>
            <tr>
                <td><?php echo $i + 1?></td>
                <td><?php _h($c['name'])?></td>
                <td><?php _h($c['description'])?></td>
                <td><?php _d($c['modified'],true, true)?></td>
                <td><?php _d($c['created'],true, true)?></td>
                <td><a href="<?php echo Router::url(array('controller' => 'AdChapter', 'action' => 'edit',$ebook['Ebook']['ebook_id'], $c['chapter_id']))?>">Sửa</a> |
                    <a href="<?php echo Router::url(array('controller' => 'AdChapter', 'action' => 'delete', $ebook['Ebook']['ebook_id'], $c['chapter_id']))?>" data-text="<?php _h($ebook['Ebook']['title'] .' - '.$c['name'])?>" data-action="delete" >Xóa</a></td>
            </tr>
        <?php endforeach?>
    </table>
</div>