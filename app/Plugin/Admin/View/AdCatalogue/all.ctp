<div class="c-container">
    <?php echo $this->Session->flash();?>
    <?php echo $this->Form->create('Catalogue', array('class' => 'small-form'));?>
    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nhập tên mục mới', 'placeholder' => 'Nhập tên mục mới'))?>
    <?php echo $this->Form->input('parent_id', array('class' => 'form-control', 'label' => 'Chọn mục cha','options' => $catalogues, 'empty' => 'Không có'))?>
    <?php echo $this->Form->submit('Thêm mục', array('class' => 'btn btn-primary'));?>
    <?php echo $this->Form->end();?>
    <div>
    <?php foreach($catalogues as $i => $c):?>
        <?php _h($c)?> <a data-text= "<?php _h($c)?>" data-action="delete" href="<?php echo Router::url(array('controller' => 'AdCatalogue', 'action' => 'delete', $i ))?>">Xóa</a><br>
        <?php endforeach;?>
    </div>
</div>