<div class="c-container">
    <a href="<?php echo Router::url(array('controller' => 'AdUser', 'action' => 'add'))?>" class="btn btn-info">Thêm người dùng mới</a>
    <br><br>
    <table class="table table-bordered table-hover">
        <tr>
            <th>#</th>
            <th>Username</th>
            <th>Email</th>
            <th>Group</th>
            <th>Fullname</th>
            <th>Ngày sửa</th>
            <th>Ngày tạo</th>
            <th>!</th>
        </tr>
        <?php foreach($users as $user): $u = $user['User']?>
            <tr>
                <td><?php _h($u['user_id'])?></td>
                <td><?php _h($u['username'])?></td>
                <td><?php _h($u['email'])?></td>
                <td><?php _h($groups[$u['group_id']])?></td>
                <td><?php _h($u['fullname'])?></td>
                <td><?php _d($u['modified'], true, true)?></td>
                <td><?php _d($u['created'], true, true)?></td>
                <td><a href="<?php echo Router::url(array('controller' => 'AdUser', 'action' => 'edit', $u['user_id']))?>">Sửa</a> | <a href="<?php ?>">Xóa</a></td>
            </tr>
        <?php endforeach?>
    </table>
    <?php echo $this->element('_paging')?>
</div>