<div class="c-container">
    <?php echo $this->Session->flash();?>
    <?php echo $this->Form->create('User', array('class' => 'small-form'))?>
    <?php echo $this->Form->input('username', array(
        'class' => 'form-control',
        'placeholder' => 'Nhập tên người dùng'))?>

    <?php echo $this->Form->input('password', array(
        'class' => 'form-control',
        'type' => 'text',
        'placeholder' => 'Nhập mật khẩu'))?>

    <?php echo $this->Form->input('email', array(
        'class' => 'form-control',
        'placeholder' => 'Nhập email'))?>

    <?php echo $this->Form->input('fullname', array(
        'class' => 'form-control',
        'placeholder' => 'Nhập tên hiển thị'))?>

    <?php echo $this->Form->input('group_id', array(
        'class' => 'form-control',
        'options' => $groups,
        'placeholder' => 'Nhập tên hiển thị'))?>

    <?php echo $this->Form->submit('Add user', array('class' => 'btn btn-info'))?>
    <?php echo $this->Form->end()?>

</div>