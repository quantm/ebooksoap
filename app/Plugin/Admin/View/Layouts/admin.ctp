<!DOCTYPE html>

<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow">
    <title><?php echo empty($title)?"Untitled":$title?></title>
    <?php echo $this->Html->css(array(
        "/lib/bootstrap/css/___bootstrap",
        'Admin.app.admin.css'
    ));?>

    <?php
    //custom css
    if(!empty($css))
        echo $this->Html->css($css);
    ?>
</head>
<body>
<?php echo $this->element("top_menu");?>

    <?php echo $this->element("left_menu");?>

<div class="container m-container">
    <?php echo $content_for_layout;?>
</div>


<?php echo $this->Html->script(array(
    "/lib/jquery/jquery-1.10.2.min.js",
    "/lib/bootstrap/js/bootstrap.min.js",
    "Admin.qlib.js",
    //"/lib/bootstrap-datepicker/js/bootstrap-datepicker.js",
    //"/lib/ckeditor/std/ckeditor.js"
))?>

<?php
//custom script
if(!empty($scripts))
    echo $this->Html->script($scripts);
?>

</body>


</html>