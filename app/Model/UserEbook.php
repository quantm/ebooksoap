<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 12/2/13 7:50 PM
*/
class UserEbook extends  AppModel{
    public $primaryKey = 'user_ebook_id';
    public $useTable   = 'users_ebooks';
    
    public $belongsTo = array(
    		'Ebook' => array(
    				'className' => 'Ebook',
    				'foreignKey' => "ebook_id")
    );

    public function IsExist($user_id, $ebook_id, $return = false){
        $r = $this->find('first', array(
            'conditions'    => array('UserEbook.user_id' => $user_id, 'UserEbook.ebook_id' => $ebook_id)
        ));
		if($return){
			return empty($r) ? false : $r;
		}
        return empty($r) ? false : true;
    }
}