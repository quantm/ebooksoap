<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module:
 * @version:
 * @date: 11/27/13 6:41 PM
 */
class Chapter extends AppModel{
    public $primaryKey = 'chapter_id';

    public $validate = array(
        'name' => array(
            'between' => array(
                'rule'    => array('between', 2, 255),
                'message' => 'Tên chương phải từ 2 đến 255 kí tự'
            )
        )
    );

  /*  public $belongsTo = array(
        'Ebook' => array(
            'className' => 'Ebook',
            'foreignKey' => "ebook_id",
            "fields" => array("title")));*/

    public function GetChapterByEbookID($ebook_id, $fields = null){
        if(!$fields)
            $result = $this->find('all', array('conditions' => array(
                'ebook_id' => $ebook_id), 'order' => 'created asc'));
        else
            $result = $this->find('all', array(
                'conditions' => array('ebook_id' => $ebook_id), 'fields' => $fields, 'order' => 'created asc'));
        return $result;
    }
}