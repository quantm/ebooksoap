<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/27/13 7:23 PM
*/
class Catalogue extends AppModel{
    public $primaryKey = 'catalogue_id';
    public $actsAs = array('Tree');

    public $validate = array(
        'name' => array(
            'isUnique' => array(
                'rule'     => 'isUnique',
                'required' => true,
                'message'  => 'Tên mục đã tồn tại'
            ),
            'between' => array(
                'rule'    => array('between', 2, 100),
                'message' => 'Tên mục phải phải từ 2 đến 100 kí tự'
            )
        )
    );


}