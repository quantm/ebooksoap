<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 12/2/13 7:42 PM
*/
class User extends AppModel {
    public $groups = array(
        1 => 'Administrator',
        2 => 'Moderator',
        3 => 'Member'
    );
    public $primaryKey = 'user_id';

    public $validate = array(
        'username' => array(
            'between' => array(
                'rule'    => array('between', 4, 255),
                'message' => 'Mã người dùng phải từ 4 đến 255 kí tự'
            ),
            'isUnique' => array(
                'rule'     => 'isUnique',
                'message'  => 'Tên đăng nhập đã tồn tại đã tồn tại'
            )
        ),
        'email' => array(
            'isEmail' => array(
                'rule'     => 'email',
                'message'  => 'Địa chỉ email không hợp lệ'
            ),
            'isUnique' => array(
                'rule'     => 'isUnique',
                'message'  => 'Email đã tồn tại'
            )
        ),
        'password' => array(
            'between' => array(
                'rule'    => array('between', 6, 255),
                'message' => 'Mật khẩu phải từ 6 đến 255 kí tự'
            )
        ),
        'fullname' => array(
            'between' => array(
                'rule'    => array('between', 6, 255),
                'message' => 'Tên hiển thị phải từ 6 đến 255 kí tự'
            )
        )
    );

    function beforeSave($options = array())
    {
        parent::beforeSave();
        if(!empty($this->data[$this->alias]["password"])){
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data['User']['password']);
        }
    }

	public function Login($username, $password){
		$hashed_password = AuthComponent::password($password);
		
		$user = $this->find('first', array(
				'conditions' => array('username' => $username, 'password' => $hashed_password)
		));
		if(!empty($user))
			return $user;
		return false;
	}

    public $hasAndBelongsToMany = array(
        'Ebook' =>
            array(
                'className' => 'Ebook',
                'joinTable' => 'users_ebooks',
                'foreignKey' => 'user_id',
                'associationForeignKey' => 'ebook_id',
                'fields'    => array('Ebook.ebook_id', 'Ebook.title')
            )
    );
}