<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module:
 * @version:
 * @date: 12/2/13 7:42 PM
 */
class Token extends AppModel {
	public $primaryKey = 'token_id';
	
	public function CreateToken($user_id){
		/*Delete previous token*/
		$this->deleteAll(array('user_id' => $user_id));
		
		/*Generate new token*/
		$token =  String::uuid();
		$data = array(
				'value' 	=> $token,
				'user_id'	=> $user_id
		);
		$this->save($data);
		return $token;
	}
	
	public function IsValidate($token_value){
		$token = $this->find('first', array(
				'fields'	=> array('token_id', 'Token.user_id','created'),
				'conditions' => array('Token.value' => $token_value)
		));
		if(empty($token))
			return false;
		$created = $token['Token']['created'];
		$last = date("Y-m-d H:i:s", strtotime("+6 hours", strtotime($created)));
		$now = date("Y-m-d H:i:s");
		$validate = strtotime($now) < strtotime($last);
		if($validate == false){
			$this->deleteAll(array('Token.value' => $token_value ));
		}
		return $validate;
	}

	public function DeleteToken($token_value){
		$this->deleteAll(array('Token.value' => $token_value));

	}

	public function GetUserID($token_value){
		$token = $this->find('first', array(
				'fields'	=> array('Token.user_id'),
				'conditions' => array('Token.value' => $token_value)
		));
		if(empty($token))
			return 0;
		return $token['Token']['user_id'];
	}
}