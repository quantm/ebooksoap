<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/27/13 7:24 PM
*/
class Author extends AppModel{
    public $primaryKey = 'author_id';

    public $validate = array(
        'name' => array(
            'isUnique' => array(
                'rule'     => 'isUnique',
                'required' => true,
                'message'  => 'Tên tác giả đã tồn tại'
            ),
            'between' => array(
                'rule'    => array('between', 2, 100),
                'message' => 'Tên tác giả phải từ 2 đến 100 kí tự'
            )
        )
    );

    public function GetAuthorList(){
        $authors = array();
        $temp = $this->find('all');
        foreach($temp as $t){
            $authors[$t['Author']['author_id']] = $t['Author']['name'];
        }
        return $authors;
    }
}