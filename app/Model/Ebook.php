<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/27/13 6:41 PM
*/
class Ebook extends AppModel{
    public $primaryKey = 'ebook_id';

    public $validate = array(
        'title' => array(
            'between' => array(
                'rule'    => array('between', 2, 255),
                'message' => 'Tên sách phải từ 2 đến 255 kí tự'
            ),
            'isUnique' => array(
                'rule'     => 'isUnique',
                'required' => true,
                'message'  => 'Tiêu đề sách đã tồn tại'
            )
        ),
        'description' => array(
            'between' => array(
                'rule'    => array('between', 2, 255),
                'message' => 'Tóm tắt của sách phải từ 2 đến 255 kí tự'
            )
        )
    );

    public $belongsTo = array(
        'Author' => array(
            'className' => 'Author',
            'foreignKey' => "author_id",
            "fields" => array("author_id", "name")),
        'Catalogue' => array(
            'className' => 'Catalogue',
            'foreignKey' => "catalogue_id",
            "fields" => array('catalogue_id','name'))
    );

    public $hasMany = array(
        'Chapter' => array(
            'className'     => 'Chapter'
        )
    );

    public function GetChapterIndex($ebook_id, $increase = false){
        $this->recursive = -1;
        $this->id = $ebook_id;
        $index = $this->field('chapter_index');
        if($increase){
            $this->saveField('chapter_index', $index + 1);
        }
        return $index;

    }

    public function UpdateChapterNum($id, $Chapter){
        $this->id = $id;
        $this->saveField('chapters_number', $Chapter->find('count', array('conditions' => array('ebook_id' => $id))));
    }
    
    public function IncreaseView($id){
    	$this->id = $id;
    	$tableName = empty($this->tablePrefix) ? $this->table : "{$this->tablePrefix}_{$this->table}";
    	$sql = "UPDATE  `{$tableName}`
				SET `views_number` = `views_number` + 1
				WHERE `ebook_id` = {$id}";
    	return $this->query($sql);
    }

}