<?php
class ApiController extends AppController{
	
	public $uses = array('Ebook', 'Chapter', 'Catalogue', 'Token', 'User', 'UserEbook');
	public $autoLayout = false;
	public $autoRender = false;
	public $components = array(
			'Paginator',
			'Session',
			'Cookie',
			'Auth'
	);
	
	public function index(){
		echo '<h1>Index page</h1>';
	}
	
	public function Ebook($ebook_id, $debug = null){
		$this->Ebook->unbindModel(array('hasMany' => array('Chapter')));			
		$ebook = $this->Ebook->read(null, $ebook_id);
		if(!empty($ebook)){
			$this->Ebook->IncreaseView($ebook_id);
			if($debug)
				pr($ebook);
			else	
				ajax($ebook);
			
		}
	}
	
	public function Content($ebook_id, $get_size = null){
		$chapters = $this->Chapter->GetChapterByEbookID($ebook_id);
		$results = array();
		foreach ($chapters as $chapter){
			$results[] = $chapter['Chapter'];
		}
		if($get_size){
			echo strlen(json_encode($results));
			return;
		}
		ajax($results); 
	}

	public function GetEbooks($param = null){
		$order = '';
		if($param == 'lastest'){
			$order = 'Ebook.created desc';
		}
		if($param == 'top_views'){
			$order = 'Ebook.views_number desc';
		}
		$this->Paginator->settings = array(
				'fields' => array('title', 'ebook_id', 'views_number','img', 'description','Author.name', 'Catalogue.name'),
				'conditions' => array('status' => 1),
				'limit' => 10,
				'order' => $order
		);
		$this->Ebook->unbindModel(array('hasMany' => array('Chapter')));
		$ebooks = $this->Paginator->paginate('Ebook');
		//pr($ebooks);
		ajax($ebooks);
		
	}

	public function GetCatalogues(){
		$catalogues = $this->Catalogue->find('all', array('fields' => array('catalogue_id', 'name', 'parent_id')));
		$results = array();
		foreach($catalogues as $c){
			$results[] = $c['Catalogue'];
		}
		//pr($results)
		ajax($results);
	}

	public function GetEbooksByCatalogue($catalogue_id, $param = null){
		$order = '';
		if($param == 'lastest'){
			$order = 'Ebook.created desc';
		}
		if($param == 'top_views'){
			$order = 'Ebook.views_number desc';
		}
		$this->Paginator->settings = array(
				'fields' => array('title','views_number', 'ebook_id', 'img', 'description','Author.name', 'Catalogue.name'),
				'conditions' => array('status' => 1, 'Ebook.catalogue_id' => $catalogue_id),
				'limit' => 10,
				'order' => $order
		);
		$this->Ebook->unbindModel(array('hasMany' => array('Chapter')));
		$ebooks = $this->Paginator->paginate('Ebook');
		ajax($ebooks);
	}

	public function GetEbooksByAuthor($author_id, $param = null){
		$order = '';
		if($param == 'lastest'){
			$order = 'Ebook.created desc';
		}
		if($param == 'top_views'){
			$order = 'Ebook.views_number desc';
		}
		$this->Paginator->settings = array(
				'fields' => array('title', 'views_number','ebook_id', 'img', 'description','Author.name', 'Catalogue.name'),
				'conditions' => array('status' => 1, 'author_id' => $author_id),
				'limit' => 10,
				'order' => $order
		);
		$this->Ebook->unbindModel(array('hasMany' => array('Chapter')));
		$ebooks = $this->Paginator->paginate('Ebook');
		ajax($ebooks);
	}

	/*User Area*/
	public function Login($username, $password){
		$user = $this->User->Login($username, $password);
		if($user !== false){
			echo $this->Token->CreateToken($user['User']['user_id']);
		}else{
			echo 'FAILED';
		}
	}

	public function IsLoggedIn($token_value){
		if($this->Token->IsValidate($token_value)){
			echo 'TRUE';
		}else{
			echo 'FALSE';
		}
	}
	
	public function Logout($token_value){
		$this->Token->DeleteToken($token_value);
	}

	public function LoadEbooksList($token_value = null, $param = null){
		$user_id = $this->Token->GetUserID($token_value);
		if($user_id){
			$order = '';
			if($param == 'lastest'){
				$order = 'Ebook.created desc';
			}
			if($param == 'top_views'){
				$order = 'Ebook.views_number desc';
			}
			
			$this->Paginator->settings = array(
				 'fields' => array( 'Ebook.ebook_id'), 
				'conditions' => array('status' => 1, 'UserEbook.user_id' => $user_id),
				'limit' => 10,
				'order' => $order
			);
			$ebook_id_list = array();
			
			$results = $this->Paginator->paginate('UserEbook');
			foreach ($results as $r){
				$ebook_id_list[] = $r['Ebook']['ebook_id'];
			}
			$this->Ebook->unbindModel(array('hasMany' => array('Chapter')));
			$ebooks = $this->Ebook->find('all', array(
					'fields' => array('title', 'views_number', 'ebook_id', 'img', 'description','Author.name', 'Catalogue.name'),
					'conditions' => array('ebook_id' => $ebook_id_list)));
			//pr($ebooks);
			ajax($ebooks);
		}
	}
	
	public function HasEbook($token, $ebook_id){
		$user_id = $this->Token->GetUserID($token);
        if($user_id == 0){
            echo 'FAILED';
            return;
        }
		if($this->UserEbook->IsExist($user_id, $ebook_id)){
            echo 'EXIST';
            return;
        }
		echo 'NONE';
	}
	
    public function AddToFavorite($token, $ebook_id){
        $user_id = $this->Token->GetUserID($token);
        if($user_id == 0){
            echo 'FAILED';
            return;
        }

        $ebook = $this->Ebook->read(null, $ebook_id);
        if(empty($ebook)){
            echo 'FAILED';
            return;
        }
        if($this->UserEbook->IsExist($user_id, $ebook_id)){
            echo 'EXIST';
            return;
        }
        $data = array(
            'ebook_id' => $ebook_id,
            'user_id'  => $user_id
        );
        if($this->UserEbook->save($data)){
            echo 'SUCCESS';
            return;
        }
        echo 'FAILED';
    }

	public function SetCurrentChapter($token, $ebook_id, $current_chapter){
		$user_id = $this->Token->GetUserID($token);
        if($user_id == 0){
            echo 'FAILED';
            return;
        }
		if($this->UserEbook->IsExist($user_id, $ebook_id)){
            $r = $this->UserEbook->updateAll(
			array('current_chapter' => $current_chapter), 
			array(
				'UserEbook.user_id' => $user_id,
				'UserEbook.ebook_id' => $ebook_id
			));
			if($r){
				echo 'SUCCESS';
			}
            return;
        }
		echo 'FAILED';
	}
	
	public function GetCurrentChapter($token, $ebook_id){
		$user_id = $this->Token->GetUserID($token);
        if($user_id == 0){
            echo 'FAILED';
            return;
        }
		$r = $this->UserEbook->IsExist($user_id, $ebook_id, true);
		if($r){
			echo $r['UserEbook']['current_chapter'];
			return;
		}
		echo 'FAILED';
	}
	
	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow();
	}
}