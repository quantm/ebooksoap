<?php
/**
 * @author: quantm.tb@gmail.com/quandaso
 * @module: 
 * @version: 
 * @date: 11/4/13 8:32 PM
*/
define("PLUGIN_NAME", "Zik.");
define('SECURE_SALT_VALUE', '_!zik.vn!_');

function ajax_tag($tag_name, $value){
    echo "<" . $tag_name . ">" . $value . "</".$tag_name.">";
}


function _h($s){
    echo htmlspecialchars($s);
}

function _n($n){
    echo number_format($n);
}

function _d($d, $H = false, $relate = false){
    //$format = $H ? "d-m-Y H:i" : "d-m-Y";
    if($relate){
        $today = date('d-m-Y', time());
        $yesterday = date('d-m-Y', strtotime("-1 days"));
        $date = date('d-m-Y', strtotime($d));
        if($today == $date){
            echo "Hôm nay, " . date('H:i', strtotime($d));
            return;
        }
        if($yesterday == $date){
            echo "Hôm qua, " . date('H:i', strtotime($d));
            return;
        }
    }
    echo date($H ? "d-m-Y, H:i" : "d-m-Y", strtotime($d));
}

function is_active($url, $target){
    echo strpos($url, $target) ? 'class="active"': '';
}

function remove_sign($s){
    $str = trim(strtolower($s));
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'a', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'e', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'i', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'o', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'u', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'y', $str);
    $str = preg_replace("/(Đ)/", 'd', $str);
    return $str;
}

function _url($id, $title){
    return  '/' . $id . '/' . generate_url($title);
}

function generate_url($title){
    return trim(preg_replace('/(\W+)/', '-', remove_sign($title)), '-') . '.html';
}

function get_upload_directory($base_directory){
    $year = date("Y");
    $month = date("m");
    if(!file_exists($base_directory)){
        mkdir($base_directory);
    }
    if(!file_exists($base_directory.DS. $year)){
        mkdir($base_directory.DS. $year);
    }
    if(!file_exists($base_directory.DS. $year.DS.$month)){
        mkdir($base_directory.DS. $year.DS.$month);
    }
    return $base_directory.DS. $year.DS.$month;
}

function ajax($result, $isDie = false) {
	try {
		ob_clean();
	} catch (Exception $e) {
	}

	echo json_encode($result);

	try {
		ob_end_flush();
	} catch (Exception $e) {
	}

	if ($isDie) {
		die();
	}
}

function make_id($length = 6) {
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}