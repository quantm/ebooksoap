<?php
	//Router::connect('/admin/post/*', array('plugin' => 'Admin', 'controller' => 'AdPost', 'action' => 'index'));
Router::connect('/admin', array('plugin' => 'Admin', 'controller' => 'AdHome', 'action' => 'index'));
Router::connect('/admin/ebooks/:action/*', array('plugin' => 'Admin', 'controller' => 'AdEbook'));
Router::connect('/admin/catalogue/:action/*', array('plugin' => 'Admin', 'controller' => 'AdCatalogue'));
Router::connect('/admin/author/:action/*', array('plugin' => 'Admin', 'controller' => 'AdAuthor'));
Router::connect('/admin/chapter/:action/*', array('plugin' => 'Admin', 'controller' => 'AdChapter'));
Router::connect('/admin/user/:action/*', array('plugin' => 'Admin', 'controller' => 'AdUser'));
Router::connect('/api/:action/*', array('controller' => 'Api'));
	CakePlugin::routes();

	require CAKE . 'Config' . DS . 'routes.php';
