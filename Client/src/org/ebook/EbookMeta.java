package org.ebook;

public class EbookMeta {
	public int ID = 0;
	public String Title = "";
	public String Description = "";
	public String Author = "";
	public String Catalogue = "";
	public String Image = "";
	public int    View	  = 0;
	public EbookMeta(int id, String title, String description, String image, String author, String catalogue, int view){
		this.ID = id;
		this.Title = title;
		this.Description = description;
		this.Image = image;
		this.View = view;
		this.Author = author;
		this.Catalogue = catalogue;
	}

}
