package org.ebook;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Feed {

	public Feed(){
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Màn hình chính gồm 3 tab:
			Tab1: Truyện mới nhất; Feed::getLastestEbook(1)
			Tab2: Truyện nhiều người đọc nhất; Feed::getTopViewEbook(1);
			Tab3: Phân loại: Feed:getCatalogues()
		*/
		Feed f = new Feed();
		//Lấy truyện mới nhất theo trang
		int page = 1;
		ArrayList<EbookMeta> ebooks = f.getLastestEbook(page);
		//Sử dụng kết quả trả về
		int index = 0;
		EbookMeta result = ebooks.get(index);
		pr(result.Title + result.Author + result.Description  );
		
		/*
		 * Lấy Catalogue
		 * */
		 
		ArrayList<Catalogue> c = f.getCatalogues();
		//Lấy sách của 1 catalogue
		int catalogue_id = c.get(0).ID;
		ArrayList<EbookMeta> e = f.getEbookByCatalogue(catalogue_id);
		pr(e.get(0).Title);
		 //Lấy thông tin sách không có nội dung
		 
		/*Hiển thị ở màn hình Preview*/
		Ebook ebook =  new Ebook(1);
		
		//Lấy kích thước nội dung trước khi tải
		int size  = ebook.getContentSize();
		
		
		
		//Lấy nội dung sách
		ebook.loadChapters();
		//Lấy chapter theo thứ tự
		 index = 0;//Chương thứ nhất
		String content 		= ebook.getChapterContent(index); //Nội dung 
		String name    		= ebook.getChapterName(index);//Tên chương
		String description	= ebook.getChapterDescription(index);//Mô tả
		pr(name + content + description);
		
	}
	
	public  ArrayList<EbookMeta> getLastestEbook(int page){
		String s = Misc._call("GetEbooks/lastest/page:" +  String.valueOf(page));
		return Misc.Json2EbookMetas(s);		
	}
	
	public  ArrayList<EbookMeta> getTopViewEbook(int page){
		String s = Misc._call("GetEbooks/top_views/page:" +  String.valueOf(page));
		return Misc.Json2EbookMetas(s);		
	}
	
	public ArrayList<Catalogue> getCatalogues(){
		String s = Misc._call("GetCatalogues");
		ArrayList<Catalogue> catalogues = new ArrayList<>();
		Catalogue catalogue = null;
		try{
			JSONParser parser = new JSONParser();
			JSONArray  result = (JSONArray )  parser.parse(s);	
			//pr(s);
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> iterator = result.iterator();
			while (iterator.hasNext()) {
				JSONObject temp = iterator.next();
				int id = Integer.parseInt(temp.get("catalogue_id").toString());
				String name = (String) temp.get("name");
				catalogue = new Catalogue(id, name);
				catalogues.add(catalogue);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return catalogues;
	}

	public ArrayList<EbookMeta> getEbookByCatalogue(int catalogue_id){
		String s = Misc._call("GetEbooksByCatalogue/" +  String.valueOf(catalogue_id));
		return Misc.Json2EbookMetas(s);	
	}
	

	
	public static void pr(Object object){
		System.out.println(object);
	}

}
