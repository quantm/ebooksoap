package org.ebook;

import java.util.ArrayList;
import java.lang.Exception;
public class User {

	/**
	 * @param args
	 */
	private String token = null;
	@SuppressWarnings("unchecked")
	private ArrayList<EbookMeta> ebooks = new ArrayList();
	
	@SuppressWarnings("unchecked")
	public ArrayList<EbookMeta> getEbooksList(){
		return (ArrayList<EbookMeta>) this.ebooks.clone();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		User u = new User();
		u.logIn("admin", "admin");
		u.loadEbooksList();
		 ArrayList<EbookMeta> r = u.getEbooksList();
		 pr(r.size());
	}
	
	public boolean logIn(String username, String password){
		String result = Misc._call("Login/" + username + '/' + password);
		if(result != "FAILED"){
			this.token = result;
			return true;
		}
		return false;
	}
	
	public boolean isLoggedIn(){
		String result = Misc._call("IsLoggedIn/" + this.token);
		return (result == "TRUE") ? true : false;
	}
	
	public void logOut(){
		Misc._call("Logout/" + this.token);
	}
	
	public void loadEbooksList() {
		String s = Misc._call("LoadEbooksList/" + this.token);
		this.ebooks = Misc.Json2EbookMetas(s);	
	}
	
	public static void pr(Object object){
		System.out.println(object);
	}

}
