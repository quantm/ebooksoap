package org.ebook;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Misc {

	public static final String SERVER_URI = "http://localhost/ebook/api/";
	public static String readURL(String url){
		String result = "";	
		try{
			URL yahoo = new URL(url);
		    URLConnection yc = yahoo.openConnection();
		    BufferedReader in = new BufferedReader(
		                            new InputStreamReader(
		                            yc.getInputStream()));
		    String inputLine = "";
	    
		    while ((inputLine = in.readLine()) != null) 
		    	result += inputLine;
		    in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	    return result;
	}

	public static String _call(String uri){
		return readURL(SERVER_URI + uri);
	}
	
	public static ArrayList<EbookMeta> Json2EbookMetas(String json_str){
		EbookMeta ebook = null;
		ArrayList<EbookMeta> results = new ArrayList<>();
		try{
			JSONParser parser = new JSONParser();
			JSONArray  result = (JSONArray )  parser.parse(json_str);		
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> iterator = result.iterator();
			while (iterator.hasNext()) {
				JSONObject temp = iterator.next();
				JSONObject _ebook = (JSONObject) temp.get("Ebook");
				JSONObject _catalogue = (JSONObject) temp.get("Catalogue");
				JSONObject _author = (JSONObject) temp.get("Author");
				int id				= Integer.parseInt(_ebook.get("ebook_id").toString());
				String title 		= (String) _ebook.get("title");
				String description 	= (String) _ebook.get("description");
				String image 		= (String) _ebook.get("img");
				String author 			= (String) _author.get("name");
				String catalogue 			= (String) _catalogue.get("name");
				int view 			= Integer.parseInt(_ebook.get("views_number").toString());
				ebook = new EbookMeta(id, title, description, image, author, catalogue, view);
				results.add(ebook);
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return results;
	}
}
