package org.ebook;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
 

public class Ebook extends EbookFields{
	
	
	
	/*!Container for ebook content*/
	private ArrayList<String> chapter_contents = new ArrayList<String>();
	private ArrayList<String> chapter_names = new ArrayList<String>();
	private ArrayList<String> chapter_descriptions = new ArrayList<String>();
	private int content_size = 0;
	public String[] getChapter(int index){
		String[] r = new String[3];
		r[0]	= this.chapter_names.get(index);
		r[1]	= this.chapter_descriptions.get(index);
		r[2]	= this.chapter_contents.get(index);
		
		return r;
	}
	public String getChapterName(int index){
		return this.chapter_names.get(index);
	}
	
	public String getChapterDescription(int index){
		return this.chapter_descriptions.get(index);
	}
	
	public String getChapterContent(int index){
		return this.chapter_contents.get(index);
	}
	
	public int getContentSize(){
		String s = Misc._call("Content/" + String.valueOf(this.ID) + "/1");
		this.content_size = Integer.parseInt(s);
		return this.content_size;
	}
	
	public String downloadImage(){
		String fname = String.valueOf(this.ID) + ".jpg";
		boolean r = Misc.downloadFile(Misc.SERVER_IMG_URI + fname, fname);
		if(r)
			return fname;
		return "";
	}
	
	public void loadChapters(){
		String s = Misc._call("Content/" + String.valueOf(this.ID));
		try{
			JSONParser parser = new JSONParser();
			JSONArray  results = (JSONArray )  parser.parse(s);
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> iterator = results.iterator();
			while (iterator.hasNext()) {
				JSONObject temp = iterator.next();
				this.chapter_names.add((String) temp.get("name"));
				this.chapter_descriptions.add((String) temp.get("description"));
				this.chapter_contents.add((String) temp.get("content"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Lấy thông tin sách(Không có nội dung từ server)
	 * @param ebook_id
	 * 
	 */
	public Ebook(int ebook_id){
		String s = Misc._call("Ebook/" + String.valueOf(ebook_id));
		this.ID = ebook_id;	
		try{
			JSONParser parser = new JSONParser();
			JSONObject result = (JSONObject)  parser.parse(s);			
			
			JSONObject _ebook = (JSONObject) result.get("Ebook");
			JSONObject _catalogue = (JSONObject) result.get("Catalogue");
			JSONObject _author = (JSONObject) result.get("Author");
			
			/*!Get ebook fields*/
			this.title 				= (String) _ebook.get("title");
			this.description 		= (String) _ebook.get("description");
			this.translator 		= (String) _ebook.get("translator");
			this.image 				= (String) _ebook.get("img");
			this.status 			= Integer.parseInt(_ebook.get("status").toString());
			this.pages_number 		= Integer.parseInt(_ebook.get("pages_number").toString());
			this.views_number 		= Integer.parseInt( _ebook.get("views_number").toString());
			this.chapters_number 	= Integer.parseInt(_ebook.get("chapters_number").toString());
			this.author_id 			= Integer.parseInt( _ebook.get("author_id").toString());
			this.catalogue_id 		= Integer.parseInt( _ebook.get("catalogue_id").toString());
			this.publish_date 		= (String) _ebook.get("publish_date");
			this.modified 			= (String) _ebook.get("modified");
			this.created 			= (String) _ebook.get("created");
			this.author 			= (String) _author.get("name");
			this.catalogue 			= (String) _catalogue.get("name");
						
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ebook ebook = new Ebook(2);
		int s = ebook.getContentSize();
		pr(s);
	}
	
	public static void pr(Object object){
		System.out.println(object);
	}
	
	
	

}
