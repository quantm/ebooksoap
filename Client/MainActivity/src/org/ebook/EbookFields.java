package org.ebook;

public class EbookFields {

	protected int ID = 0;
	protected String title = "";
	protected String description = "";
	protected String translator = "";	
	protected String image = "";
	protected int status = 0;
	protected int pages_number = 0;
	protected int views_number = 0;
	protected int chapters_number = 0;
	protected int author_id = 0;
	protected int catalogue_id = 0;
	protected String author = "";
	protected String catalogue = "";
	protected String publish_date = "";
	protected String modified = "";
	protected String created = "";
	
	public String getTitle(){return this.title;}	
	public String getDescription(){return this.description;}
	public String getTranslator(){return this.translator;}
	public String getImage(){return this.image;}
	public String getAuthor(){return this.author;}
	public String getCatalogue(){return this.catalogue;}
	public String getPublishDate(){return this.publish_date;}
	public String getModifiedTime(){return this.modified;}
	public String getCreatedTime(){return this.created;}
	public int getID(){return this.ID;}
	public int getStatus(){return this.status;}
	public int getPagesNumber(){return this.pages_number;}
	public int getViewsNumber(){return this.views_number;}
	public int getChaptersNumber(){return this.chapters_number;}
	public int getAuthorID(){return this.author_id;}
	public int getCatalogueID(){return this.catalogue_id;}
	
}
