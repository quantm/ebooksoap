package org.ebook;

import java.util.ArrayList;
import java.lang.Exception;

import android.util.Log;
public class User {

	/**
	 * @param args
	 */
	private String token = null;
	@SuppressWarnings("unchecked")
	private ArrayList<EbookMeta> ebooks = new ArrayList();
	
	@SuppressWarnings("unchecked")
	public ArrayList<EbookMeta> getEbooksList(){
		return (ArrayList<EbookMeta>) this.ebooks.clone();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		User u = new User();
		u.logIn("admin", "admin");
		u.loadEbooksList();
		 ArrayList<EbookMeta> r = u.getEbooksList();
		 pr(r.size());
	}
	
	public boolean logIn(String username, String password){
		String result = Misc._call("Login/" + username + '/' + password);
		if(!result.equals("FAILED")){
			this.token = result;		
			return true;
			
		}
		return false;
	}
	
	public boolean isLoggedIn(){
		String result = Misc._call("IsLoggedIn/" + this.token);
		return result.equals("TRUE") ? true : false;
	}
	
	public boolean addToFavorite(int ebook_id){	
		String result = Misc._call("AddToFavorite/" + this.token + "/" + String.valueOf(ebook_id));
		return result.equals("SUCCESS") ? true : false;
	}
	
	public boolean hasEbook(int ebook_id){
		String result = Misc._call("HasEbook/" + this.token + "/" + String.valueOf(ebook_id));
		return result.equals("EXIST") ? true : false;
	}
	
	public boolean setCurrentChapter(int ebook_id, int current_chapter){
		String result = Misc._call("SetCurrentChapter/" + this.token + "/" + String.valueOf(ebook_id)
				+ "/" + String.valueOf(current_chapter));
		return result.equals("SUCCESS") ? true : false;
	}
	public int getCurrentChapter(int ebook_id){
		String result = Misc._call("GetCurrentChapter/" + this.token + "/" + String.valueOf(ebook_id));
		if(!result.equals("FAILED"))
			return Integer.valueOf(result);
		return -1;
	}
	
	public void logOut(){
		Misc._call("Logout/" + this.token);
	}
	
	public void loadEbooksList() {
		String s = Misc._call("LoadEbooksList/" + this.token);
		this.ebooks = Misc.Json2EbookMetas(s);	
	}
	
	public static void pr(Object object){
		System.out.println(object);
	}

}
