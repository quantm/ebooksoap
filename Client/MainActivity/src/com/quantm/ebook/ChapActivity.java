package com.quantm.ebook;

import java.util.ArrayList;
import java.util.HashMap;

import org.ebook.EbookMeta;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class ChapActivity extends ListActivity {
	public static ArrayList<EbookMeta> listEbook;
	private ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
	private ListActivity activity;
	private static int current_chapter = 0;
	private ProgressDialog dialog;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cat_main);
		activity = this;
				
		if(EbookActivity.isLogin){
			new Thread(){
				public void run(){					
					ChapActivity.current_chapter = EbookActivity.mUser.getCurrentChapter(PrivewActivity.ID);
					for (int i = 0; i < PrivewActivity.mEbook.getChaptersNumber(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						if(ChapActivity.current_chapter == i + 1){
							map.put("name", PrivewActivity.mEbook.getChapterName(i) + " - reading");
						}else{
							map.put("name", PrivewActivity.mEbook.getChapterName(i));
						}
						map.put("content", PrivewActivity.mEbook.getChapterContent(i));
						map.put("description", PrivewActivity.mEbook.getChapterDescription(i));
						list.add(map);
					}
					final BaseAdapter adapter = new SimpleAdapter(activity, list, R.layout.item_cat, new String[] {"name"},
							new int[] {R.id.item_cat_name});
					activity.runOnUiThread(new Runnable() {
						public void run() {
							activity.setListAdapter(adapter);
						}
					});
				}
			}.start();
		}else{
			for (int i = 0; i < PrivewActivity.mEbook.getChaptersNumber(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("name", PrivewActivity.mEbook.getChapterName(i));
				map.put("content", PrivewActivity.mEbook.getChapterContent(i));
				map.put("description", PrivewActivity.mEbook.getChapterDescription(i));
				list.add(map);
			}
			final BaseAdapter adapter = new SimpleAdapter(activity, list, R.layout.item_cat, new String[] {"name"},
					new int[] {R.id.item_cat_name});
			activity.runOnUiThread(new Runnable() {
				public void run() {
					activity.setListAdapter(adapter);
				}
			});
		}
	}

	public void onListItemClick(ListView parent, View vi, int position,long id) {
		ViewTextActivity.content = list.get(position).get("content");
		ChapActivity.current_chapter = position + 1;
		if(EbookActivity.isLogin){
			new Thread(){
				public void run(){					
					EbookActivity.mUser.setCurrentChapter(PrivewActivity.ID, ChapActivity.current_chapter);
				}
			}.start();
		}
		dialog = new ProgressDialog(this);
		dialog.show();
		startActivity(new Intent(this, ViewTextActivity.class));
		dialog.cancel();
	}
}
