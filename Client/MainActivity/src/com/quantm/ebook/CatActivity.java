package com.quantm.ebook;

import java.util.ArrayList;
import java.util.HashMap;

import org.ebook.Ebook;
import org.ebook.EbookMeta;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class CatActivity extends ListActivity {
	public static ArrayList<EbookMeta> listEbook;
	private Activity activity;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cat_main);
		activity = this;
		
		ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
		for (int i = 0; i < listEbook.size(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", listEbook.get(i).Title);
			map.put("author", listEbook.get(i).Author + "  - lượt xem:  " + listEbook.get(i).View);
			map.put("description", listEbook.get(i).Description);
			list.add(map);
		}
		BaseAdapter adapter = new SimpleAdapter(this, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
				new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
		this.setListAdapter(adapter);
	}

	
	public void onListItemClick(ListView parent, View vi, int position,long id) {
		PrivewActivity.ID = listEbook.get(position).ID;
		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.show();
		new Thread(){
			public void run(){
				PrivewActivity.mEbook = new Ebook(PrivewActivity.ID);
				PrivewActivity.txName = PrivewActivity.mEbook.getTitle();
				PrivewActivity.txAuthor = PrivewActivity.mEbook.getAuthor();
				PrivewActivity.txCatalogue = PrivewActivity.mEbook.getCatalogue();
				PrivewActivity.txDescription = PrivewActivity.mEbook.getDescription();
				PrivewActivity.txView = PrivewActivity.mEbook.getViewsNumber();
				dialog.cancel();
				
				startActivity(new Intent(activity, PrivewActivity.class));
			}
		}.start();
	}
}
