package com.quantm.ebook;

import java.util.ArrayList;
import java.util.HashMap;

import org.ebook.Catalogue;
import org.ebook.Ebook;
import org.ebook.EbookMeta;
import org.ebook.Feed;
import org.ebook.User;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class EbookActivity extends ListActivity {
	public static Feed mFeed = new Feed();
	public static User mUser = new User();
	public static ArrayList<EbookMeta> listEbook;
	public static ArrayList<EbookMeta> userListEbook;
	public static ArrayList<Catalogue> listCat;
	public static boolean isLogin = false;
	private static int flagButtonOld = 1;
	
	private int flagButton = 1;	// 1 - New		2 - Top		3- Cat		4-User
	
	private Button mButtonNew, mButtonTop, mButtonCat, mButtonUser;
	private BaseAdapter adapter;
	private ProgressDialog dialog;
	private ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
	private ListActivity activity;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ebook_main);
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		activity = this;
		isLogin = false;
		flagButtonOld = 1;
		flagButton = 1;
		
		mButtonNew = (Button) findViewById(R.id.ebook_new);
		mButtonTop = (Button) findViewById(R.id.ebook_top);
		mButtonCat = (Button) findViewById(R.id.ebook_cat);
		mButtonUser = (Button) findViewById(R.id.ebook_user);

		for (int i = 0; i < listEbook.size(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("name", listEbook.get(i).Title);
			map.put("author", listEbook.get(i).Author + "  -  lượt xem:  " + listEbook.get(i).View);
			map.put("description", listEbook.get(i).Description);
			list.add(map);
		}
		
		mButtonNew.setSelected(true);
		mButtonCat.setSelected(false);
		mButtonTop.setSelected(false);
		mButtonUser.setSelected(false);
		
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT);
		params.weight = 0;
		mButtonUser.setLayoutParams(params);
		
		adapter = new SimpleAdapter(this, list, R.layout.item_ebook,
				new String[] {"name", "author", "description"},
				new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
		setListAdapter(adapter);
	}
	
	public void ebookNew(View v){
		dialog = new ProgressDialog(this);
		dialog.show();
		new Thread(){
			public void run(){
				list.clear();
				flagButton = 1;
				listEbook = mFeed.getLastestEbook(1);
				for (int i = 0; i < listEbook.size(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("name", listEbook.get(i).Title);
					map.put("author", listEbook.get(i).Author + "  - lượt xem:  " + listEbook.get(i).View);
					map.put("description", listEbook.get(i).Description);
					list.add(map);
				}
				activity.runOnUiThread(new Runnable() {
					public void run() {
						
						mButtonNew.setSelected(true);
						mButtonCat.setSelected(false);
						mButtonTop.setSelected(false);
						mButtonUser.setSelected(false);
						adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
								new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
						activity.setListAdapter(adapter);
						if(dialog != null && dialog.isShowing()){
							dialog.cancel();
						}
					}
				});
			}
		}.start();
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.menu_login, menu);
        return true;
    }
	
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
    	case R.id.menulogin:
    		// Login
    		if(!isLogin){
    			startActivity(new Intent(activity, LoginActivity.class));
    		}else{
    			Toast.makeText(this, "Bạn đã đăng nhập thành công!", Toast.LENGTH_LONG).show();
    		}
    		break;
    	case R.id.menulogout:
    		// Logout
    		if(isLogin){
	    		isLogin = false;
	    		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT);
	    		params.weight = 0;
	    		mButtonUser.setLayoutParams(params);
	    		
	    		if(flagButton == 4){
					list.clear();
		    		flagButton = flagButtonOld;
					switch (flagButton) {
					case 1:
						mButtonNew.setSelected(true);
						mButtonCat.setSelected(false);
						mButtonTop.setSelected(false);
						mButtonUser.setSelected(false);
						for (int i = 0; i < listEbook.size(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("name", listEbook.get(i).Title);
							map.put("author", listEbook.get(i).Author + "  -  lượt xem:  " + listEbook.get(i).View);
							map.put("description", listEbook.get(i).Description);
							list.add(map);
						}
						adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
								new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
						break;
					case 2:
						mButtonNew.setSelected(false);
						mButtonCat.setSelected(false);
						mButtonTop.setSelected(true);
						mButtonUser.setSelected(false);
						for (int i = 0; i < listEbook.size(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("name", listEbook.get(i).Title);
							map.put("author", listEbook.get(i).Author + "  -  lượt xem:  " + listEbook.get(i).View);
							map.put("description", listEbook.get(i).Description);
							list.add(map);
						}
						adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
								new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
						break;
					case 3:
						mButtonNew.setSelected(false);
						mButtonCat.setSelected(true);
						mButtonTop.setSelected(false);
						mButtonUser.setSelected(false);
						for (int i = 0; i < listCat.size(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("name", listCat.get(i).Name);
							list.add(map);
						}
						adapter = new SimpleAdapter(activity, list, R.layout.item_cat, new String[] {"name"}, new int[] {R.id.item_cat_name});
						break;
		
					default:
						break;
					}
					activity.setListAdapter(adapter);
	    		}
    		}else{
    			Toast.makeText(this, "Bạn chưa đăng nhập!", Toast.LENGTH_LONG).show();
    		}
    		break;
        }
        return true;
    }
	
	public void onStart(){
		if(isLogin){
			list.clear();
			if(flagButton != 4){
				flagButtonOld = flagButton;
			}
			flagButton = 4;
			for (int i = 0; i < userListEbook.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("name", userListEbook.get(i).Title);
				map.put("author", userListEbook.get(i).Author + "  -  lượt xem:  " + userListEbook.get(i).View);
				map.put("description", userListEbook.get(i).Description);
				list.add(map);
			}
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, mButtonCat.getHeight());
			params.weight = 1;
			mButtonUser.setLayoutParams(params);
			mButtonNew.setSelected(false);
			mButtonCat.setSelected(false);
			mButtonTop.setSelected(false);
			mButtonUser.setSelected(true);

			adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
					new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
			activity.setListAdapter(adapter);
		}
		super.onStart();
	}
	
	public void ebookTop(View v){
		dialog = new ProgressDialog(this);
		dialog.show();
		new Thread(){
			public void run(){
				list.clear();
				flagButton = 2;
				listEbook = mFeed.getTopViewEbook(1);
				for (int i = 0; i < listEbook.size(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("name", listEbook.get(i).Title);
					map.put("author", listEbook.get(i).Author + "  -  lượt xem:  " + listEbook.get(i).View);
					map.put("description", listEbook.get(i).Description);
					list.add(map);
				}
				activity.runOnUiThread(new Runnable() {
					public void run() {
						if(dialog != null && dialog.isShowing()){
							dialog.cancel();
						}
						mButtonNew.setSelected(false);
						mButtonCat.setSelected(false);
						mButtonTop.setSelected(true);
						mButtonUser.setSelected(false);
						adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
								new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
						activity.setListAdapter(adapter);
					}
				});
			}
		}.start();
	}
	
	public void ebookCat(View v){
		dialog = new ProgressDialog(this);
		dialog.show();
		new Thread(){
			public void run(){
				list.clear();
				flagButton = 3;
				listCat = mFeed.getCatalogues();
				for (int i = 0; i < listCat.size(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("name", listCat.get(i).Name);
					list.add(map);
				}
				
				activity.runOnUiThread(new Runnable() {
					public void run() {
						if(dialog != null && dialog.isShowing()){
							dialog.cancel();
						}
						mButtonNew.setSelected(false);
						mButtonCat.setSelected(true);
						mButtonTop.setSelected(false);
						mButtonUser.setSelected(false);
						adapter = new SimpleAdapter(activity, list, R.layout.item_cat, new String[] {"name"}, new int[] {R.id.item_cat_name});
						activity.setListAdapter(adapter);
					}
				});
			}
		}.start();
	}
	
	public void ebookUser(View v){
		list.clear();
		if(flagButton != 4){
			flagButtonOld = flagButton;
		}
		flagButton = 4;
		if(this.isLogin){
			dialog = new ProgressDialog(this);
			dialog.show();
			new Thread(){
				public void run(){
					EbookActivity.mUser.loadEbooksList();
					EbookActivity.userListEbook = EbookActivity.mUser.getEbooksList();
					for (int i = 0; i < userListEbook.size(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("name", userListEbook.get(i).Title);
						map.put("author", userListEbook.get(i).Author + "  -  lượt xem:  " + userListEbook.get(i).View);
						map.put("description", userListEbook.get(i).Description);
						list.add(map);
					}
					activity.runOnUiThread(new Runnable() {
						public void run() {
							LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, mButtonCat.getHeight());
							params.weight = 1;
							mButtonUser.setLayoutParams(params);
							mButtonNew.setSelected(false);
							mButtonCat.setSelected(false);
							mButtonTop.setSelected(false);
							mButtonUser.setSelected(true);

							adapter = new SimpleAdapter(activity, list, R.layout.item_ebook, new String[] {"name", "author", "description"},
									new int[] {R.id.item_ebook_name, R.id.item_ebook_author, R.id.item_ebook_description});
							activity.setListAdapter(adapter);
							if(dialog != null && dialog.isShowing()){
								dialog.cancel();
							}				
						}
					});
				}
			}.start();
		}
		
		
	}
	
	public void onListItemClick(ListView parent, View vi, final int position,long id) {
		switch (flagButton) {
		case 1:
		case 2:
		case 4:
			dialog = new ProgressDialog(this);
			dialog.show();
			new Thread(){
				public void run(){
					if(flagButton == 4){
						PrivewActivity.ID = userListEbook.get(position).ID;
					}else{
						PrivewActivity.ID = listEbook.get(position).ID;
					}
					PrivewActivity.mEbook = new Ebook(PrivewActivity.ID);
					PrivewActivity.txName = PrivewActivity.mEbook.getTitle();
					PrivewActivity.txAuthor = PrivewActivity.mEbook.getAuthor();
					PrivewActivity.txCatalogue = PrivewActivity.mEbook.getCatalogue();
					PrivewActivity.txDescription = PrivewActivity.mEbook.getDescription();
					PrivewActivity.txView = PrivewActivity.mEbook.getViewsNumber();
					
					
					startActivity(new Intent(activity, PrivewActivity.class));
					dialog.cancel();
				}
			}.start();
			break;
		case 3:
			dialog = new ProgressDialog(this);
			dialog.show();
			new Thread(){
				public void run(){
					CatActivity.listEbook = mFeed.getEbookByCatalogue(listCat.get(position).ID);
					
					if(CatActivity.listEbook.size() > 0){
						startActivity(new Intent(activity, CatActivity.class));
					}
					dialog.cancel();
				}
			}.start();
			break;

		default:
			break;
		}
	}	
}
