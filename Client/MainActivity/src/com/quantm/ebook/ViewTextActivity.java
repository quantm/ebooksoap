package com.quantm.ebook;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ViewTextActivity extends Activity {
	public static String content;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ebook_text);
		
		TextView mTextView = (TextView) findViewById(R.id.text_ebook);
		mTextView.setText(content);
	}

}
