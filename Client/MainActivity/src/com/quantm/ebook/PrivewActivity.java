package com.quantm.ebook;

import org.ebook.Ebook;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PrivewActivity extends Activity {
	public static int ID;
	private TextView name, author, catalogue, description, view;
	private Button addToFavoriteBtn;
	public static Ebook mEbook;
	public static String txName, txAuthor, txCatalogue, txDescription;
	public static int txView;
	private ProgressDialog dialog;
	private Activity activity;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ebook_preview);
		activity = this;
		
		name = (TextView) findViewById(R.id.preview_name);
		author = (TextView) findViewById(R.id.preview_author);
		catalogue = (TextView) findViewById(R.id.preview_catalogue);
		description = (TextView) findViewById(R.id.preview_description);
		view = (TextView) findViewById(R.id.preview_views);
		addToFavoriteBtn = (Button) findViewById(R.id.add_favorite);	
		addToFavoriteBtn.setVisibility(View.GONE);
		name.setText(txName);
		author.setText("Tác giả:   " + txAuthor);
		catalogue.setText("Chuyên mục:   " + txCatalogue);
		description.setText("Thông tin:   " + txDescription);
		view.setText("Lượt xem:   " + txView);
		if(EbookActivity.isLogin){
			new Thread(){
				public void run(){
					boolean success = EbookActivity.mUser.hasEbook(PrivewActivity.ID);
					
					if(success){
						addToFavoriteBtn.setVisibility(View.VISIBLE);
						addToFavoriteBtn.setEnabled(false);
						addToFavoriteBtn.setText("Đã thêm");
					}else{
						addToFavoriteBtn.setVisibility(View.VISIBLE);
					}
				}
			}.start();
		}
	}
	
	public void viewText(View v){
		dialog = new ProgressDialog(this);
		dialog.show();
		new Thread(){
			public void run(){
				PrivewActivity.mEbook.loadChapters();				
				startActivity(new Intent(activity, ChapActivity.class));
				dialog.cancel();
			}
		}.start();
	}
	
	public void AddToFavorite(View v){
		new Thread(){
			public void run(){
				boolean r = EbookActivity.mUser.addToFavorite(PrivewActivity.ID);
				if(r){
					activity.runOnUiThread(new Runnable() {
					    public void run() {
					    	addToFavoriteBtn.setVisibility(View.VISIBLE);
							addToFavoriteBtn.setEnabled(false);
							addToFavoriteBtn.setText("Đã thêm");
					    }
					});
					
				}else{
					//addToFavoriteBtn.setVisibility(View.VISIBLE);
				}
			}
		}.start();
	}
	

}
