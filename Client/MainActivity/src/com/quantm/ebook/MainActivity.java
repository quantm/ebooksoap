package com.quantm.ebook;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {
	private Activity activity;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root_main);
		activity = this;
		new LoadEbooks();
	}
	
	private class LoadEbooks implements Runnable{
		private Thread mThread;
		public LoadEbooks(){
			mThread = new Thread(this);
			mThread.start();
		}
		@SuppressWarnings("static-access")
		public void run() {
			try {
				mThread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			EbookActivity.listEbook = EbookActivity.mFeed.getLastestEbook(1);
			startActivity(new Intent(activity, EbookActivity.class));
			activity.finish();
		}
		
	}
}
