package com.quantm.ebook;

import org.ebook.User;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	private EditText mUserText, mPassText;
	private ProgressDialog dialog;
	private Activity activity ;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_main);
		mUserText = (EditText) findViewById(R.id.login_user);
		mPassText = (EditText) findViewById(R.id.login_pass);
		activity = this;
	}
	
	public void login(View v){
		
		if(mUserText.getText().toString() != null && mUserText.getText().toString().length() > 0){
			dialog = new ProgressDialog(this);
			dialog.show();
			new Thread(){
				public void run(){
					
					boolean l = EbookActivity.mUser.logIn(mUserText.getText().toString(), mPassText.getText().toString());
					Log.e("fuck",mUserText.getText().toString() +  mPassText.getText().toString());
					if(l){
						EbookActivity.mUser.loadEbooksList();
						EbookActivity.userListEbook = EbookActivity.mUser.getEbooksList();
						EbookActivity.isLogin = true;
						finish();
					}else{
						activity.runOnUiThread(new Runnable() {
						    public void run() {
						        Toast.makeText(activity, "Tên đăng nhập hoặc mật khẩu không hợp lệ", Toast.LENGTH_SHORT).show();
						        dialog.hide();
						    }
						});
					}
					
				}
			}.start();
		}else{
			Toast.makeText(this, "Vui lòng điền đầy đủ thông tin !", Toast.LENGTH_LONG).show();
			
		}
	}

}
