/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.quantm.ebook;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int button_tab=0x7f020000;
        public static final int icon=0x7f020001;
        public static final int menu_login=0x7f020002;
        public static final int menu_logout=0x7f020003;
        public static final int vpi__tab_selected_focused_holo=0x7f020004;
        public static final int vpi__tab_selected_holo=0x7f020005;
        public static final int vpi__tab_selected_pressed_holo=0x7f020006;
        public static final int vpi__tab_unselected_focused_holo=0x7f020007;
        public static final int vpi__tab_unselected_holo=0x7f020008;
        public static final int vpi__tab_unselected_pressed_holo=0x7f020009;
    }
    public static final class id {
        public static final int add_favorite=0x7f05000d;
        public static final int ebook_cat=0x7f050002;
        public static final int ebook_new=0x7f050000;
        public static final int ebook_top=0x7f050001;
        public static final int ebook_user=0x7f050003;
        public static final int item_cat_name=0x7f05000f;
        public static final int item_ebook_author=0x7f050011;
        public static final int item_ebook_description=0x7f050012;
        public static final int item_ebook_name=0x7f050010;
        public static final int login_pass=0x7f050015;
        public static final int login_user=0x7f050014;
        public static final int menulogin=0x7f050016;
        public static final int menulogout=0x7f050017;
        public static final int name_cat=0x7f050013;
        public static final int preview_author=0x7f050008;
        public static final int preview_catalogue=0x7f050009;
        public static final int preview_description=0x7f05000b;
        public static final int preview_image=0x7f050006;
        public static final int preview_layout1=0x7f050005;
        public static final int preview_name=0x7f050007;
        public static final int preview_view=0x7f050004;
        public static final int preview_views=0x7f05000a;
        public static final int preview_viewtext=0x7f05000c;
        public static final int progressBar1=0x7f050018;
        public static final int text_ebook=0x7f05000e;
    }
    public static final class layout {
        public static final int cat_main=0x7f030000;
        public static final int chap_main=0x7f030001;
        public static final int ebook_main=0x7f030002;
        public static final int ebook_preview=0x7f030003;
        public static final int ebook_text=0x7f030004;
        public static final int item_cat=0x7f030005;
        public static final int item_ebook=0x7f030006;
        public static final int listview=0x7f030007;
        public static final int login_main=0x7f030008;
        public static final int menu_login=0x7f030009;
        public static final int root_main=0x7f03000a;
    }
    public static final class string {
        public static final int app_name=0x7f040001;
        public static final int hello=0x7f040000;
    }
}
